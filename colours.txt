RED
===

officially:
pantone Rubine Red 2X CVC    RGB 215, 10, 83

other colours
pantone #206   	      	     RGB 206,  0, 55
pantone 485C Red	     RGB 218, 41, 28
pantone 32c  		     RGB 239, 51, 64
